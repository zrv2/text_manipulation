import re

def find_non_terminal_said(fname):

    pattern = re.compile('said')
    
    with open(fname) as f:
        a = f.readlines()
        sum = 0
        for x in a: 
            total = len(re.findall('said', x))
            total_w = len(re.findall('said\.', x))
            sum += (total-total_w)
        print(sum)


if __name__ == "__main__":
    find_non_terminal_said('example_text.txt')
