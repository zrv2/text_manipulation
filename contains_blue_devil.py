
def contains_blue_devil(fname):

    with open(fname) as f:

        a = f.readlines()
        count = 0

        for x in a:
            curr = x.count('Blue Devil')
            count += curr
        
        print(count > 0)

if __name__ == "__main__":
    
    contains_blue_devil('example_text.txt')
